<?php
namespace Extranet\TTOMBackOfficeMarchandBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(
 *     db="Extranet_ceesto",
 *     collection="TTOMBOMarchand___Shop"
 * )
 */
class Shop
{
	/**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

	/** @MongoDB\Field(type="string") */
    protected $name;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    protected $image;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $front;



	/** @MongoDB\Field(type="date") */
    protected $created;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Extranet\DashboardBundle\Document\User")
     */
    private $users;

    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getUsers()
    {
        return ($this->users);
    }
    
    /**
    * @param Plugin $plugin
    */
    public function addUser(\Extranet\DashboardBundle\Document\User $user){
	    $this->users[] = $user;
    }

    public function clearUsers(){
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(){return ($this->id);}

    public function getName(){return ($this->name);}

    public function getImage(){return ($this->image);}

    public function getFront(){return ($this->front);}

	public function getCreated(){return ($this->created);}

    public function setName($value){$this->name=$value;}

    public function setImage($value){$this->image=$value;}

    public function setFront($value){$this->front=$value;}

    public function setCreated(){$this->created=date('Y-m-d');}

}

?>