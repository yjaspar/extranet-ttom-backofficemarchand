<?php
namespace Extranet\TTOMBackOfficeMarchandBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(
 *     db="Extranet_ceesto",
 *     collection="TTOMBOMarchand___Bill"
 * )
 */
class Bill
{
	/**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /** @MongoDB\Field(type="string") */
    protected $devise;

    /** @MongoDB\Field(type="string") */
    protected $billNumber;

    /** @MongoDB\Field(type="string") */
    protected $tva;

    /** @MongoDB\Field(type="string") */
    protected $startPoint;

    /** @MongoDB\Field(type="string") */
    protected $endPoint;

    /** @MongoDB\Field(type="string") */
    protected $paymentMode;

    /** @MongoDB\Field(type="date") */
    protected $paymentDate;

    /** @MongoDB\Field(type="date") */
    protected $date;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $client;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $references;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $package;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $billing;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\TTOMBackOfficeMarchandBundle\Document\Shop")
     */
    private $shop;
    
    public function getId(){return ($this->id);}
    public function getTVA(){return ($this->tva);}
    public function getDevise(){return ($this->devise);}
    public function getBillNumber(){return ($this->billNumber);}
    public function getStartPoint(){return ($this->startPoint);}
    public function getEndPoint(){return ($this->endPoint);}
    public function getDate($type=null){
        if ($type=='string')
            return (date("d/m/Y", $this->date->getTimestamp()));
    }
    public function getClient(){return ($this->client);}
    public function getReferences(){return ($this->references);}
    public function getPackage(){return ($this->package);}
    public function getBilling(){return ($this->billing);}
    public function getShop(){return ($this->shop);}
    public function getPaymentMode(){return ($this->paymentMode);}
    public function getPaymentDate($type=null){
        if ($type=='string')
            return (date("d/m/Y", $this->paymentDate->getTimestamp()));
    }

    public function setDevise($value){$this->devise = $value;}
    public function setTVA($value){$this->tva = $value;}
    public function setBillNumber($value){$this->billNumber = $value;}
    public function setStartPoint($value){$this->startPoint = $value;}
    public function setEndPoint($value){$this->endPoint = $value;}
    public function setDate($value){$this->date = $value;}
    public function setClient($value){$this->client = $value;}
    public function setReferences($value){$this->references = $value;}
    public function setPackage($value){$this->package = $value;}
    public function setBilling($value){$this->billing = $value;}
    public function setShop($value){$this->shop = $value;}
    public function setPaymentMode($value){$this->paymentMode = $value;}
    public function setPaymentDate($value){$this->paymentDate = $value;}
    
}

?>