toDelete = new Array();

$(document).ready(function(){
	$('#display-add').click(function(){
	    if($('.bg-forms').is(':hidden')){
	            $('.bg-forms').slideDown('fast');
	    }else{
	            $('.bg-forms').slideUp('fast');
	    }
    });

	$('.button.bg-color-grayDark.fg-color-white').click(function(){
		url=$(this).attr('href');
		$.Dialog({
		    'title'      : '<span style="color: #fff">{{ "Do you really want to delete" | trans }} '+$(this).parent().find('h2').text()+'?</span>',
		    'content'    : '<small>{{ "Once deleted, you cannot get it back ..." | trans }}</small>',
		    'buttons'    : {
		        '{{ "Cancel" | trans }}'    : {
		            'action': function(){return false;}
		        },
		        '{{ "Delete" | trans }}'     : {
		            'action': function(){document.location.href=url;}
		        }
		    }
		});
	});

	function deleteElement(email){
		for(x=0;x<=toDelete.length;x++){
			if (toDelete[x]==email && x>0){
				for(y=x;y>0;y--){
					tmp=toDelete[y-1];
					toDelete[y-1]=toDelete[y];
					toDelete[y]=tmp;
				}
			}
		}
		toDelete.shift();
	}


    $('.tile.double.outline-color-white.selectable').click(function(){
		if ($(this).attr('class').match(/selected/)){
			deleteElement($(this).find('.hidden-id').attr('value'));
			$(this).removeClass('selected');
		}else{
			toDelete.push($(this).find('.hidden-id').attr('value'));
			$(this).addClass('selected');
		}
		console.log(toDelete.length);
		if (toDelete.length==1)
			$('.command-button').removeAttr('disabled');
		else if(toDelete.length==0)
			$('.command-button').attr('disabled', '');
	});
});