<?php

namespace Extranet\TTOMBackOfficeMarchandBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller
{

    public function indexAction()
    {
    	$dm = $this->get('doctrine_mongodb')->getManager();
        if ($_POST){
            if (isset($_POST['id']))
                $shop = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')->findOneById(new \MongoId($_POST['id']));
            else
                $shop = new \Extranet\TTOMBackOfficeMarchandBundle\Document\Shop();
            if ($_FILES['image']['tmp_name']!=''){
                $size = getimagesize($_FILES['image']['tmp_name']);
            
                $upload = new \Extranet\DashboardBundle\Document\Upload();
                $upload->setHeight($size[1]);
                $upload->setWidth($size[0]);
                $upload->setFile($_FILES['image']['tmp_name']);
                $upload->setFileName($_FILES['image']['name']);
                $upload->setMimeType($_FILES['image']['type']);
                $upload->setLength($_FILES['image']['size']);

                $dm->persist($upload);
                $dm->flush();
                $shop->setImage($upload);
                $shop->setName($_POST['name']);
            }
            $shop->setFront(array('state' => false, 'updated' => new \MongoDate()));
            $shop->setName($_POST['name']);
            $dm->persist($shop);
            $dm->flush();
        }

        if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') || $this->get('security.context')->isGranted('ROLE_ADMIN')){
            $shops = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')->findAll();
            $bills = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Bill')->findAll();
        }else{
            $dm = $this->get('doctrine_mongodb')->getManager();
            $qb = $dm->createQueryBuilder('ExtranetTTOMBackOfficeMarchandBundle:Shop');
            $qb->field('users')->elemMatch($qb->expr()->field('$id')->equals(new \MongoId($this->get('security.context')->getToken()->getUser()->getId())));
            $shops = $qb->getQuery()->execute();

            $qb = $dm->createQueryBuilder('ExtranetTTOMBackOfficeMarchandBundle:Bill');
            foreach($shops as $shop)
                $qb->field('shop.$id')->equals(new \MongoId($shop->getId()));
            $bills = $qb->getQuery()->execute();
        }
    	return $this->render('ExtranetTTOMBackOfficeMarchandBundle:Default:index.html.twig', array('admin' => ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')||$this->get('security.context')->isGranted('ROLE_ADMIN')), 'self' => $this->get('security.context')->getToken()->getUser(), 'title' => 'Back Office Marchand - TTOM', 'shops' => $shops, 'bills' => $bills));
    }

    public function statusShopAction($id){
        $dm = $this->get('doctrine_mongodb')->getManager();
        $request = Request::createFromGlobals();
        if ($request->isXmlHttpRequest()){
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent('{"responseCode":200, "responseText":"Success"}');

            $shop = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')->findOneById(new \MongoId($id));
            $shop->setFront(array('state' => true, 'updated' => new \MongoDate()));

            $dm->persist($shop);
            $dm->flush();

            return $response;
        }else{
            $shop = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')->findOneById(new \MongoId($id));
            $shop->setFront(array('state' => false, 'updated' => new \MongoDate(), 'why' => $_POST['explain-us']));

            $dm->persist($shop);
            $dm->flush();
            return $this->redirect($this->generateUrl('showShop', array('id'=>$id)));
        }
    }

    public function showShopAction($id){
        $dm = $this->get('doctrine_mongodb')->getManager();
        $request = Request::createFromGlobals();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        preg_match('/\/extranet\/(.*)\/Shop/', $_SERVER['REQUEST_URI'], $matches);

        if ($request->isXmlHttpRequest()){
            $user = $dm->getRepository('ExtranetDashboardBundle:User')->findOneByEmail($_POST['email']);
            $isAllowed=false;
            if ($user){
                $plugins=$user->getPlugins();
                foreach($plugins as $plugin){
                    if ($plugin->getSlug() == $matches[1])
                        $isAllowed=true;

                }
                if ($isAllowed){
                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $shop = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')
                            ->find($id);
                    $shop->addUser($user);
                    $dm->flush();
                    $response->setContent('{"responseCode":200, "responseText":"Success"}');
                }else
                    $response->setContent('{"responseCode":404, "responseText":"Not found"}');
            }else
                $response->setContent('{"responseCode":404, "responseText":"Not found"}');
         
            return $response;
        }else{
            $shop = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')->findById($id);

            return $this->render('ExtranetTTOMBackOfficeMarchandBundle:Default:showShop.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'title' => 'Back Office Marchand - TTOM', 'shop' => $shop->getNext()));
        }
    }

    public function deleteShopAction($id){
        $dm = $this->get('doctrine_mongodb')->getManager();
        $request = Request::createFromGlobals();

        $response = new Response();
        $response->headers->set('Content-Type', 'text/html');
        
        $ids = json_decode($_POST['ids']);

        if ($request->isXmlHttpRequest()){
            $shop = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Shop')->findOneById($id);
            $tmp_users = $shop->getUsers();
            $shop->clearUsers();
            
            foreach($tmp_users as $key => $user){
                $set=true;
                foreach($ids as $id)
                    if ($id == $user->getId()){$set=false;}
                if ($set){$shop->addUser($user);}
            }
            
            $dm->flush();
            $response->setContent('{"responseCode":200, "responseText":"Success"}');
         
            return $response;
        }else{
            return $this->render('ExtranetTTOMBackOfficeMarchandBundle:Default:showShop.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'title' => 'Back Office Marchand - TTOM'));
        }
    }

    public function printBillAction($id){
        $dm = $this->get('doctrine_mongodb')->getManager();

        $pdfGenerator = $this->get('siphoc.pdf.generator');
        $pdfGenerator->setName($id.'.pdf');

        $bill = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Bill')->findOneById($id);

        $totalBill = array('totalHT' => 0);
        $b = $bill->getBilling();
        foreach($b as $k => $v){$totalBill['totalHT'] += $v['price'];}
        $totalBill['tvaToPay'] = round(($totalBill['totalHT'] * $bill->getTVA())/100, 2);
        $totalBill['totalTTC'] = round(($totalBill['totalHT'] + $totalBill['tvaToPay']), 2);

        //return $this->render('ExtranetTTOMBackOfficeMarchandBundle:Default:printBill.html.twig', array('bill' => $bill, 'totalBill' => $totalBill));
        return $pdfGenerator->downloadFromView('ExtranetTTOMBackOfficeMarchandBundle:Default:printBill.html.twig', array('bill' => $bill, 'totalBill' => $totalBill));
    }

    public function displayBillAction($id){
        $dm = $this->get('doctrine_mongodb')->getManager();

        $bill = $dm->getRepository('ExtranetTTOMBackOfficeMarchandBundle:Bill')->findOneById($id);

        $totalBill = array('totalHT' => 0);
        $b = $bill->getBilling();
        foreach($b as $k => $v){$totalBill['totalHT'] += $v['price'];}
        $totalBill['tvaToPay'] = round(($totalBill['totalHT'] * $bill->getTVA())/100, 2);
        $totalBill['totalTTC'] = round(($totalBill['totalHT'] + $totalBill['tvaToPay']), 2);

        return $this->render('ExtranetTTOMBackOfficeMarchandBundle:Default:dispBill.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(),'bill' => $bill, 'totalBill' => $totalBill, 'title' => 'Back Office Marchand - TTOM'));
    }
}
